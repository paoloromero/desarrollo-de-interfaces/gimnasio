<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "musculos".
 *
 * @property int $id
 * @property string|null $codigo_maquina
 * @property string|null $musculos_trabajan
 *
 * @property Maquinas $codigoMaquina
 */
class Musculos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'musculos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo_maquina'], 'string', 'max' => 5],
            [['musculos_trabajan'], 'string', 'max' => 20],
            [['codigo_maquina', 'musculos_trabajan'], 'unique', 'targetAttribute' => ['codigo_maquina', 'musculos_trabajan']],
            [['codigo_maquina'], 'exist', 'skipOnError' => true, 'targetClass' => Maquinas::className(), 'targetAttribute' => ['codigo_maquina' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'codigo_maquina' => 'Codigo Maquina',
            'musculos_trabajan' => 'Musculos Trabajan',
        ];
    }

    /**
     * Gets query for [[CodigoMaquina]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoMaquina()
    {
        return $this->hasOne(Maquinas::className(), ['codigo' => 'codigo_maquina']);
    }
}
