<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "asisten".
 *
 * @property int $id_asisten
 * @property int|null $nsocio_cliente
 * @property string|null $codigo_clase
 *
 * @property Clases $codigoClase
 * @property Clientes $nsocioCliente
 */
class Asisten extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'asisten';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nsocio_cliente'], 'integer'],
            [['codigo_clase'], 'string', 'max' => 4],
            [['nsocio_cliente', 'codigo_clase'], 'unique', 'targetAttribute' => ['nsocio_cliente', 'codigo_clase']],
            [['codigo_clase'], 'exist', 'skipOnError' => true, 'targetClass' => Clases::className(), 'targetAttribute' => ['codigo_clase' => 'codigo']],
            [['nsocio_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['nsocio_cliente' => 'n_socio']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_asisten' => 'Id Asisten',
            'nsocio_cliente' => 'Nsocio Cliente',
            'codigo_clase' => 'Codigo Clase',
        ];
    }

    /**
     * Gets query for [[CodigoClase]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoClase()
    {
        return $this->hasOne(Clases::className(), ['codigo' => 'codigo_clase']);
    }

    /**
     * Gets query for [[NsocioCliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNsocioCliente()
    {
        return $this->hasOne(Clientes::className(), ['n_socio' => 'nsocio_cliente']);
    }
}
