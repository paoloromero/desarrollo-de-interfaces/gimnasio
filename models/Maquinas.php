<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "maquinas".
 *
 * @property string $codigo
 * @property string|null $tipo
 * @property string|null $dni_empleados
 *
 * @property Empleados $dniEmpleados
 * @property Musculos[] $musculos
 * @property Clientes[] $nsocioClientes
 * @property Utilizan[] $utilizans
 */
class Maquinas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'maquinas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'required'],
            [['codigo'], 'string', 'max' => 5],
            [['tipo'], 'string', 'max' => 25],
            [['dni_empleados'], 'string', 'max' => 9],
            [['codigo'], 'unique'],
            [['dni_empleados'], 'exist', 'skipOnError' => true, 'targetClass' => Empleados::className(), 'targetAttribute' => ['dni_empleados' => 'dni']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'tipo' => 'Tipo',
            'dni_empleados' => 'Dni Empleados',
        ];
    }

    /**
     * Gets query for [[DniEmpleados]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniEmpleados()
    {
        return $this->hasOne(Empleados::className(), ['dni' => 'dni_empleados']);
    }

    /**
     * Gets query for [[Musculos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMusculos()
    {
        return $this->hasMany(Musculos::className(), ['codigo_maquina' => 'codigo']);
    }

    /**
     * Gets query for [[NsocioClientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNsocioClientes()
    {
        return $this->hasMany(Clientes::className(), ['n_socio' => 'nsocio_cliente'])->viaTable('utilizan', ['codigo_maquina' => 'codigo']);
    }

    /**
     * Gets query for [[Utilizans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUtilizans()
    {
        return $this->hasMany(Utilizan::className(), ['codigo_maquina' => 'codigo']);
    }
}
