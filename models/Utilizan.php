<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "utilizan".
 *
 * @property int $id_utilizan
 * @property string|null $codigo_maquina
 * @property int|null $nsocio_cliente
 *
 * @property Maquinas $codigoMaquina
 * @property Clientes $nsocioCliente
 */
class Utilizan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'utilizan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nsocio_cliente'], 'integer'],
            [['codigo_maquina'], 'string', 'max' => 5],
            [['codigo_maquina', 'nsocio_cliente'], 'unique', 'targetAttribute' => ['codigo_maquina', 'nsocio_cliente']],
            [['nsocio_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['nsocio_cliente' => 'n_socio']],
            [['codigo_maquina'], 'exist', 'skipOnError' => true, 'targetClass' => Maquinas::className(), 'targetAttribute' => ['codigo_maquina' => 'codigo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_utilizan' => 'Id Utilizan',
            'codigo_maquina' => 'Codigo Maquina',
            'nsocio_cliente' => 'Nsocio Cliente',
        ];
    }

    /**
     * Gets query for [[CodigoMaquina]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoMaquina()
    {
        return $this->hasOne(Maquinas::className(), ['codigo' => 'codigo_maquina']);
    }

    /**
     * Gets query for [[NsocioCliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNsocioCliente()
    {
        return $this->hasOne(Clientes::className(), ['n_socio' => 'nsocio_cliente']);
    }
}
