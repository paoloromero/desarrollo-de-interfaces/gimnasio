<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clases".
 *
 * @property string $codigo
 * @property string|null $nombre
 * @property string|null $horario
 * @property string|null $duracion
 *
 * @property Asisten[] $asistens
 * @property Clientes[] $nsocioClientes
 */
class Clases extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clases';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'required'],
            [['codigo'], 'string', 'max' => 4],
            [['nombre'], 'string', 'max' => 35],
            [['horario'], 'string', 'max' => 550],
            [['duracion'], 'string', 'max' => 40],
            [['codigo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'nombre' => 'Nombre',
            'horario' => 'Horario',
            'duracion' => 'Duracion',
        ];
    }

    /**
     * Gets query for [[Asistens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAsistens()
    {
        return $this->hasMany(Asisten::className(), ['codigo_clase' => 'codigo']);
    }

    /**
     * Gets query for [[NsocioClientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNsocioClientes()
    {
        return $this->hasMany(Clientes::className(), ['n_socio' => 'nsocio_cliente'])->viaTable('asisten', ['codigo_clase' => 'codigo']);
    }
}
