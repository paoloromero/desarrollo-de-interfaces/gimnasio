<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property int $n_socio
 * @property string|null $dni
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $email
 * @property int|null $cp
 * @property int|null $telefono
 * @property string|null $direccion
 * @property string|null $dni_entrenadores
 *
 * @property Asisten[] $asistens
 * @property Clases[] $codigoClases
 * @property Maquinas[] $codigoMaquinas
 * @property Entrenadores $dniEntrenadores
 * @property Utilizan[] $utilizans
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['n_socio'], 'required'],
            [['n_socio', 'cp', 'telefono'], 'integer'],
            [['dni', 'dni_entrenadores'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 25],
            [['apellidos'], 'string', 'max' => 30],
            [['email', 'direccion'], 'string', 'max' => 50],
            [['n_socio'], 'unique'],
            [['dni_entrenadores'], 'exist', 'skipOnError' => true, 'targetClass' => Entrenadores::className(), 'targetAttribute' => ['dni_entrenadores' => 'dni']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'n_socio' => 'N Socio',
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'email' => 'Email',
            'cp' => 'Cp',
            'telefono' => 'Telefono',
            'direccion' => 'Direccion',
            'dni_entrenadores' => 'Dni Entrenadores',
        ];
    }

    /**
     * Gets query for [[Asistens]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAsistens()
    {
        return $this->hasMany(Asisten::className(), ['nsocio_cliente' => 'n_socio']);
    }

    /**
     * Gets query for [[CodigoClases]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoClases()
    {
        return $this->hasMany(Clases::className(), ['codigo' => 'codigo_clase'])->viaTable('asisten', ['nsocio_cliente' => 'n_socio']);
    }

    /**
     * Gets query for [[CodigoMaquinas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodigoMaquinas()
    {
        return $this->hasMany(Maquinas::className(), ['codigo' => 'codigo_maquina'])->viaTable('utilizan', ['nsocio_cliente' => 'n_socio']);
    }

    /**
     * Gets query for [[DniEntrenadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDniEntrenadores()
    {
        return $this->hasOne(Entrenadores::className(), ['dni' => 'dni_entrenadores']);
    }

    /**
     * Gets query for [[Utilizans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUtilizans()
    {
        return $this->hasMany(Utilizan::className(), ['nsocio_cliente' => 'n_socio']);
    }
}
