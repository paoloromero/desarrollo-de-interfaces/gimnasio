<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "empleados".
 *
 * @property string $dni
 * @property string|null $nombre
 * @property string|null $apellidos
 *
 * @property Maquinas[] $maquinas
 */
class Empleados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'empleados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni'], 'required'],
            [['dni'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 25],
            [['apellidos'], 'string', 'max' => 30],
            [['dni'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
        ];
    }

    /**
     * Gets query for [[Maquinas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getMaquinas()
    {
        return $this->hasMany(Maquinas::className(), ['dni_empleados' => 'dni']);
    }
}
