<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entrenadores".
 *
 * @property string $dni
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property string|null $titulacion
 *
 * @property Clientes[] $clientes
 */
class Entrenadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entrenadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['dni'], 'required'],
            [['dni'], 'string', 'max' => 9],
            [['nombre'], 'string', 'max' => 25],
            [['apellidos'], 'string', 'max' => 30],
            [['titulacion'], 'string', 'max' => 80],
            [['dni'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'titulacion' => 'Titulacion',
        ];
    }

    /**
     * Gets query for [[Clientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getClientes()
    {
        return $this->hasMany(Clientes::className(), ['dni_entrenadores' => 'dni']);
    }
}
