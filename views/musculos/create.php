<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Musculos */

$this->title = 'Create Musculos';
$this->params['breadcrumbs'][] = ['label' => 'Musculos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="musculos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
