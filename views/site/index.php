<?php

/* @var $this yii\web\View */
use yii\helpers\Html;

$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="bordecarrusel text-center bg-transparent">
        <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
    <li data-target="#carouselExampleIndicators" data-slide-to="0" class="active"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="1"></li>
    <li data-target="#carouselExampleIndicators" data-slide-to="2"></li>
  </ol>
  <div class="carousel-inner">
    <div class="carousel-item active">
      <img class="d-block w-100">
      <?= Html::img("@web/images/gym3.png", ['class' => 'resize']) ?>
      <div class="carousel-caption d-none d-md-block">
          <h1>Instalaciones</h1>
      </div>
    </div>
    <div class="carousel-item">
      <img class="d-block w-100">
      <?= Html::img("@web/images/maquinas.png", ['class' => 'resize']) ?>
      <div class="carousel-caption d-none d-md-block">
          <h1>Máquinas</h1>
      </div>
    </div>
    <div class="carousel-item">
      <img class="d-block w-100">
      <?= Html::img("@web/images/clases2.png", ['class' => 'resize']) ?>
      <div class="carousel-caption d-none d-md-block">
          <h1>Clases</h1>
      </div>
    </div>
  </div>
  <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
    <span class="carousel-control-next-icon" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
        </div>
    </div>
</div>
    <div class="body-content">

        <div class="row card-deck">
            
            <div class="card text-center bordetarjeta">
                <h4 class="card-header headertarjeta">PLAN BÁSICO</h4>
                <div class="card-body">
                    <h5 class="card-title">Características</h5>
                   
                    <ul class="a">
                        <p>Acceso al gimnasio: 1 local</p>
                        <li>Mochila de regalo</li>
                        <li>Entrenador personal (1 mes de prueba)</li>
                        <li>Reservas para las clases: 2 por semana</li>
                    </ul>
                    <ul class="b">
                        <li>Máquina de bebidas</li>
                        <li>Invitar a un amigo por semana</li>
                        <li>Sesión con nutricionista</li>
                        <p></p>
                        <p><b>Cuota de inscripción: GRATIS</b></p>
                    </ul>
                    
                    <?= Html::a("19'99€", [''], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>
            
            <div class="card text-center bordetarjeta">
                <h4 class="card-header headertarjeta">PLAN ESTÁNDAR</h4>
                <div class="card-body">
                    <h5 class="card-title">Características</h5>
                    
                    <ul class="a">
                        <p>Acceso al gimnasio: +100 locales</p>
                        <li>Mochila de regalo</li>
                        <li>Entrenador personal (3 mes de prueba)</li>
                        <li>Reservas para las clases: 2 por semana</li>
                        <li>Máquina de bebidas</li>
                    </ul>
                    <ul class="b">
                        <li>Invitar a un amigo por semana</li>
                        <li>Sesión con nutricionista</li>
                        <p></p>
                        <p><b>Cuota de inscripción: GRATIS</b></p>
                    </ul>
                    
                    <?= Html::a("24,99€", [''], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>
            <div class="card text-center bordetarjeta">
                <h4 class="card-header headertarjeta">PLAN PREMIUM</h4>
                <div class="card-body">
                    <h5 class="card-title">Características</h5>
                    
                    <ul class="a">
                        <p>Acceso al gimnasio: +100 locales</p>
                        <li>Mochila de regalo</li>
                        <li>Entrenador personal (6 meses de prueba)</li>
                        <li>Reservas para las clases: 4 por semana</li>
                        <li>Máquina de bebidas</li>
                        <p></p>
                        <li>Invitar a un amigo por semana</li>
                        <li>Sesión con nutricionista</li>
                        <p></p>
                        <p><b>Cuota de inscripción: GRATIS</b></p>
                    </ul>
                  
                    <?= Html::a("39,99€", [''], ['class' => 'btn btn-warning']) ?>
                </div>
            </div>
            
            </div>
        </div>
    
