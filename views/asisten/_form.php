<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Asisten */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="asisten-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nsocio_cliente')->textInput() ?>

    <?= $form->field($model, 'codigo_clase')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
