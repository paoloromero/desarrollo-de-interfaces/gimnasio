<?php

namespace app\controllers;

use app\models\Utilizan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * UtilizanController implements the CRUD actions for Utilizan model.
 */
class UtilizanController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Utilizan models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Utilizan::find(),
            /*
            'pagination' => [
                'pageSize' => 50
            ],
            'sort' => [
                'defaultOrder' => [
                    'id_utilizan' => SORT_DESC,
                ]
            ],
            */
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Utilizan model.
     * @param int $id_utilizan Id Utilizan
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id_utilizan)
    {
        return $this->render('view', [
            'model' => $this->findModel($id_utilizan),
        ]);
    }

    /**
     * Creates a new Utilizan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Utilizan();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id_utilizan' => $model->id_utilizan]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Utilizan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $id_utilizan Id Utilizan
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id_utilizan)
    {
        $model = $this->findModel($id_utilizan);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id_utilizan' => $model->id_utilizan]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Utilizan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $id_utilizan Id Utilizan
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id_utilizan)
    {
        $this->findModel($id_utilizan)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Utilizan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $id_utilizan Id Utilizan
     * @return Utilizan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id_utilizan)
    {
        if (($model = Utilizan::findOne(['id_utilizan' => $id_utilizan])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
